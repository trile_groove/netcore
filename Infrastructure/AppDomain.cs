﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Microsoft.DotNet.PlatformAbstractions;
using Microsoft.Extensions.DependencyModel;

namespace App.Infrastructure
{
    /// <summary>
    /// http://www.michael-whelan.net/replacing-appdomain-in-dotnet-core/
    /// </summary>
    public class AppDomain
    {
        public static AppDomain CurrentDomain { get; }

        static AppDomain()
        {
            CurrentDomain = new AppDomain();
        }

        public Assembly[] GetAssemblies(string name)
        {
            var assemblies = new List<Assembly>();
            var dependencies = DependencyContext.Default.RuntimeLibraries;
            foreach (var library in dependencies)
            {
                if (IsCandidateCompilationLibrary(library, name))
                {
                    var assembly = Assembly.Load(new AssemblyName(name));
                    assemblies.Add(assembly);
                }
            }

            return assemblies.ToArray();
        }

        private static bool IsCandidateCompilationLibrary(RuntimeLibrary compilationLibrary, string name)
        {
            return compilationLibrary.Name.ToUpper() == name.ToUpper();
        }
    }
}
