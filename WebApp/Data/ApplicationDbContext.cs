﻿//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Threading.Tasks;
//using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
//using Microsoft.EntityFrameworkCore;
//using Microsoft.EntityFrameworkCore.Infrastructure;
//using Microsoft.Extensions.Configuration;
//using WebApp.Models;

//namespace WebApp.Data
//{
//    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
//    {
//        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
//            : base(options)
//        {
            
//        }

//        protected override void OnModelCreating(ModelBuilder builder)
//        {
//            base.OnModelCreating(builder);
//            // Customize the ASP.NET Identity model and override the defaults if needed.
//            // For example, you can rename the ASP.NET Identity table names and more.
//            // Add your customizations after calling base.OnModelCreating(builder);
//        }
//    }

//    public class ApplicationDbContextFactory : IDbContextFactory<ApplicationDbContext>
//    {
//        public ApplicationDbContext Create(DbContextFactoryOptions options)
//        {
//            var config = new ConfigurationBuilder()
//                .SetBasePath(options.ContentRootPath)
//                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
//                .AddJsonFile($"appsettings.{options.EnvironmentName}.json", optional: true);
//            var builder = new DbContextOptionsBuilder<ApplicationDbContext>();
//            var configuration = config.Build();

//            builder.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));
//            return new ApplicationDbContext(builder.Options);
//        }
//    }
//}
