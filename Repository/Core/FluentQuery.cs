﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using App.Entity;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using App.Entity.Core;
using LinqKit;

namespace App.Repository.Core
{
    public class FluentQuery<TEntity> : IFluentQuery<TEntity> where TEntity : BaseEntity, new()
    {
        #region Private Fields

        private IQueryable<TEntity> _query;
        private readonly Repository<TEntity> _repository;
        private bool _isSorted;

        #endregion Private Fields

        #region Constructors

        public FluentQuery(Repository<TEntity> repository)
        {
            _repository = repository;
            _query = repository.GetQueryable()/*.AsExpandable()*/; // AsExpandable has problem with eager loading
        }

        public FluentQuery(Repository<TEntity> repository, IQueryObject<TEntity> queryObject)
            : this(repository)
        {
            _query = _query.Where(queryObject.Query());
        }

        public FluentQuery(Repository<TEntity> repository, Expression<Func<TEntity, bool>> filter)
            : this(repository)
        {
            _query = _query.Where(filter);
        }

        #endregion Constructors

        public IFluentQuery<TEntity> OrderBy(Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy)
        {
            _query = orderBy(_query);
            _isSorted = true;
            return this;
        }

        public IFluentQuery<TEntity> OrderBy(string orderBy)
        {
            _query = _query.OrderBy(orderBy);
            _isSorted = true;
            return this;
        }

        public IFluentQuery<TEntity> Include(Func<IQueryable<TEntity>, IQueryable<TEntity>> include)
        {
            _query = include(_query);
            return this;
        }

        public IQueryable<TEntity> SelectPaging(int page, int pageSize, out int totalCount, out int pageCount)
        {
            if (!_isSorted)
            {
                _query = _query.OrderBy(x => x.Id);
            }

            totalCount = _query.Count();
            pageCount = (totalCount + pageSize - 1) / pageSize;
            return _query.Skip((page - 1) * pageSize).Take(pageSize);
        }

        public IQueryable<TEntity> Select(int? maxItems = null)
        {
            if (maxItems.HasValue)
                return _query.Take(maxItems.Value);

            return _query;
        }

        public IQueryable<TResult> Select<TResult>(Expression<Func<TEntity, TResult>> selector, int? maxItems = null)
        {
            if (maxItems.HasValue)
                return _query.Take(maxItems.Value).Select(selector);

            return _query.Select(selector);
        }

        //public IQueryable<TEntity> ExecuteSqlQuery(string query, params object[] parameters)
        //{
        //    return _repository.ExecuteSqlQuery(query, parameters);
        //}
    }
}
