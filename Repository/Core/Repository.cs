﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using App.DataAccess;
using App.Entity;
using App.Entity.Core;
using Microsoft.EntityFrameworkCore;

namespace App.Repository.Core
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity, new()
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly DbSet<TEntity> _dbSet;

        public Repository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
            _dbSet = dbContext.Set<TEntity>();
        }

        public virtual TEntity GetById(int id, Func<IQueryable<TEntity>, IQueryable<TEntity>> include = null)
        {
            var query = _dbSet.Where(x => x.Id == id);
            if (include != null)
            {
                query = include(query);
            }

            return query.SingleOrDefault();
        }

        public virtual async Task<TEntity> GetByIdAsync(int id, Func<IQueryable<TEntity>, IQueryable<TEntity>> include = null)
        {
            var query = _dbSet.Where(x => x.Id == id);
            if (include != null)
            {
                query = include(query);
            }

            return await query.SingleOrDefaultAsync();
        }

        //public virtual IQueryable<TEntity> ExecuteSqlQuery(string query, params object[] parameters)
        //{
        //    return _dbSet.SqlQuery(query, parameters).AsQueryable();
        //}

        public virtual void Insert(TEntity entity)
        {
            if (entity != null)
            {
                _dbSet.Add(entity);
            }
        }

        public virtual void InsertRange(IEnumerable<TEntity> entities)
        {
            if (entities != null)
            {
                var list = entities.ToList();
                _dbSet.AddRange(list);
            }
        }

        public virtual void Update(TEntity entity)
        {
            if (entity != null)
            {
                _dbSet.Update(entity);
            }
        }

        public virtual void Delete(int id)
        {
            var entity = new TEntity { Id = id };
            Delete(entity);
        }

        public virtual void Delete(TEntity entity)
        {
            if (entity == null)
                return;

            _dbSet.Attach(entity);
            _dbContext.Entry(entity).State = EntityState.Deleted;
            _dbSet.Remove(entity);
        }

        public IFluentQuery<TEntity> Query()
        {
            return new FluentQuery<TEntity>(this);
        }

        public virtual IFluentQuery<TEntity> Query(IQueryObject<TEntity> queryObject)
        {
            return new FluentQuery<TEntity>(this, queryObject);
        }

        public virtual IFluentQuery<TEntity> Query(Expression<Func<TEntity, bool>> query)
        {
            return new FluentQuery<TEntity>(this, query);
        }

        internal IQueryable<TEntity> GetQueryable()
        {
            return _dbSet;
        }

        //internal IQueryable<TEntity> Select(
        //    Expression<Func<TEntity, bool>> filter = null,
        //    Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
        //    List<Expression<Func<TEntity, object>>> includes = null,
        //    int? page = null,
        //    int? pageSize = null)
        //{
        //    IQueryable<TEntity> query = _dbSet;

        //    if (includes != null)
        //    {
        //        query = includes.Aggregate(query, (current, include) => current.Include(include));
        //    }
        //    if (orderBy != null)
        //    {
        //        query = orderBy(query);
        //    }
        //    if (filter != null)
        //    {
        //        query = query.AsExpandable().Where(filter);
        //    }
        //    if (page != null && pageSize != null)
        //    {
        //        if (orderBy == null)
        //        {
        //            query = query.OrderBy(x => x.Id);
        //        }

        //        query = query.Skip((page.Value - 1) * pageSize.Value).Take(pageSize.Value);
        //    }
        //    return query;
        //}

        //internal async Task<IEnumerable<TEntity>> SelectAsync(
        //    Expression<Func<TEntity, bool>> query = null,
        //    Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
        //    List<Expression<Func<TEntity, object>>> includes = null,
        //    int? page = null,
        //    int? pageSize = null)
        //{
        //    //See: Best Practices in Asynchronous Programming http://msdn.microsoft.com/en-us/magazine/jj991977.aspx
        //    return await Task.Run(() => Select(query, orderBy, includes, page, pageSize).AsEnumerable()).ConfigureAwait(false);
        //}
    }
}
