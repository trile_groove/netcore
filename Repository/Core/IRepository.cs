﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using App.Entity;
using App.Entity.Core;

namespace App.Repository.Core
{
    public interface IRepository<TEntity> where TEntity : BaseEntity
    {
        TEntity GetById(int id, Func<IQueryable<TEntity>, IQueryable<TEntity>> include = null);

        Task<TEntity> GetByIdAsync(int id, Func<IQueryable<TEntity>, IQueryable<TEntity>> include = null);

        ///// <summary>
        ///// Executes SQL query
        ///// </summary>
        ///// <param name="query"></param>
        ///// <param name="parameters"></param>
        ///// <returns></returns>
        //IQueryable<TEntity> ExecuteSqlQuery(string query, params object[] parameters);

        /// <summary>
        /// Inserts Entity
        /// </summary>
        /// <param name="entity"></param>
        void Insert(TEntity entity);

        /// <summary>
        /// Inserts mulitple entities
        /// </summary>
        /// <param name="entities"></param>
        void InsertRange(IEnumerable<TEntity> entities);

        /// <summary>
        /// Update entity
        /// </summary>
        /// <param name="entity"></param>
        void Update(TEntity entity);

        /// <summary>
        /// Deletes entity
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        void Delete(int id);

        /// <summary>
        /// Deletes entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        void Delete(TEntity entity);

        /// <summary>
        /// Does querying
        /// </summary>
        /// <param name="queryObject"></param>
        /// <returns></returns>
        IFluentQuery<TEntity> Query(IQueryObject<TEntity> queryObject);

        /// <summary>
        /// Does querying
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        IFluentQuery<TEntity> Query(Expression<Func<TEntity, bool>> query);

        /// <summary>
        /// Does querying
        /// </summary>
        /// <returns></returns>
        IFluentQuery<TEntity> Query();
    }
}
