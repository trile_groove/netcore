﻿using System.Collections.Generic;
using App.Entity.Core;

namespace App.Entity.ProductDomain
{
    public class Product : BaseEntity
    {
        public string Name { get; set; }

        private ICollection<ProductPhoto> _photos;
        public virtual ICollection<ProductPhoto> Photos
        {
            get { return _photos ?? (_photos = new List<ProductPhoto>()); }
            set { _photos = value; }
        }
    }
}
