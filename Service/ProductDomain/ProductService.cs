﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using App.Entity.ProductDomain;
using App.Repository.Core;
using App.Service.Core;
using Microsoft.EntityFrameworkCore;

namespace App.Service.ProductDomain
{
    public interface IProductService
    {
        Task<ProductDto> GetByIdAsync(int id);
        Task<ListResultDto<ProductDto>> ListAsync(string searchTerm, string sortData = null, int? pageIndex = null, int? pageSize = null);
        Task<ProductDto> InsertOrUpdateAsync(ProductDto dto, string username);
    }

    public class ProductService : BaseService<ProductDto, Product>, IProductService
    {
        protected override Func<IQueryable<Product>, IQueryable<Product>> IncludeExpression
        {
            get { return x => x.Include(m => m.Photos); }
        }

        public ProductService(IUnitOfWork unitOfWork, IEntityMapper<ProductDto, Product> entityMapper)
            : base(unitOfWork, entityMapper)
        {
        }

        public Task<ListResultDto<ProductDto>> ListAsync(string searchTerm, string sortData = null, int? pageIndex = null, int? pageSize = null)
        {
            return ListAsync(x => x.Name.Contains(searchTerm), sortData, pageIndex, pageSize);
        }
    }
}
