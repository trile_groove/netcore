﻿using System;
using System.Collections.Generic;
using System.Text;
using App.DataAccess.Core;
using App.Entity.ProductDomain;
using Microsoft.EntityFrameworkCore;

namespace App.DataAccess.Mappings
{
    class ProductPhotoMapping : IEntityConfiguration
    {
        public void Configure(ModelBuilder builder)
        {
            builder.Entity<ProductPhoto>().HasOne(x => x.Product).WithMany(x => x.Photos);
        }
    }
}
