﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace App.DataAccess.Core
{
    interface IEntityConfiguration
    {
        void Configure(ModelBuilder builder);
    }
}
